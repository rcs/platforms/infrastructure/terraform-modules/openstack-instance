variable "instance_count" {
  description = "Number of instances to launch"
  default     = 1
}

variable "name" {
  description = "Name to be used on all resources as prefix"
  default     = "prefix"
}

variable "image" {
  description = "Image that will be used to create instances"
}

variable "flavor" {
  description = "The Instance Flavor"
}

variable "networks" {
  description = "The network list used to attach the instance"
  default     = []
  type        = list
}

variable "port" {
  description = "The config to define create_port"
  default     = []
}

variable "cloud_init" {
  description = " Map of cloud_init containing template and variables to render"
  default     = {}
}

variable "tags" {
  description = " Map of tags to use on instance metadata"
  default     = {}
}

variable "security_group_ids" {
  description = "List of security_group_ids  to associate with instance"
  default     = null
}

variable "security_groups" {
  description = "List of security_groups to associate with instance"
  default     = null
}

variable "stop_before_destroy" {
  description = "Boolean to define if instance needs stop before destroy"
  default     = false
}


