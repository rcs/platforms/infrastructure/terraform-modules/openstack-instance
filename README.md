# Terraform Module for Managing Instances in Openstack

Want to use Terraform to manage instances within Openstack?
Then this module is for you. 


This module conforms to the [terraform standard module
structure](https://www.terraform.io/docs/modules/create.html#standard-module-structure).

Configuration variables are documented in [variables.tf](variables.tf).

## Usage Examples

The [examples](examples/) directory contains examples of use. A basic usage
example is available in [examples/main.tf](examples/main.tf).