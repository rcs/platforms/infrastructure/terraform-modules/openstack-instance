locals {
  data = {
    "environment" = var.environment
    "name"        = var.stack_name
  }
}

module "instance" {
  source = "../"
  # source = "git::ssh://git@gitlab.developers.cam.ac.uk/rcs/platforms/infrastructure/terraform-modules/openstack-instance.git?ref=master"
  # Number of instances to be created, default = 1
  instance_count = 1
  # name is a prefix to be used when instance will be created
  name = "instance_name"
  # security_groups names list to be used on instance
  security_groups = ["default"]
  # security_group_ids list to be used on port when defined
  security_group_ids = ["security_group_id"]
  # a list of networks to be used on instance create, it will iterate over list and attach the instance to multiple networks
  networks = [{
    uuid = "network_uid"
    name = "network_name"
  }]
  # port definition to create port separated from instance
  port = {

    network_id = "network_id"
    allowed_address_pairs = {
      ip_address  = "192.168.0.1/32"
      mac_address = "00:00:00:00:00:00"
    }
  }
  # image to create instance
  image = "CentOS-7-x86_64-GenericCloud"
  # define flavor to be used on instance create
  flavor = "C1.vss.tiny"
  cloud_init = {
    template = "template_path"
    vars     = "template_variables"
  }

}
