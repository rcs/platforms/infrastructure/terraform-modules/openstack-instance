terraform {
  backend "swift" {
    container         = "tf-vpc-terraform-state"
    archive_container = "tf-vpc-terraform-state-backup"
  }
}

